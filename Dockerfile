FROM openjdk:11-jdk-slim

ADD ./target/acnstudents-backend-1.0.0.jar  app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8080

