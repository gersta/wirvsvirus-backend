package org.acnstudents.backend.service;

import org.acnstudents.backend.modelrest.Body;
import org.acnstudents.backend.modelrest.InlineResponse201;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CombinedService {
    @Autowired
    private SiteManagerService siteManagerService;

    @Autowired
    private SiteService siteService;

    public InlineResponse201 saveSiteAndSiteManager(Body body) {
        int respSite = siteService.saveOrUpdate(body.getSite());
        int respSiteManager = siteManagerService.registerSiteManager(body.getSiteManager(), respSite);
        return new InlineResponse201().site(respSite).siteManager(respSiteManager);
    }
}
