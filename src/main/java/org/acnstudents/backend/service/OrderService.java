package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Order;
import org.acnstudents.backend.repository.CustomerRepo;
import org.acnstudents.backend.repository.OrderRepo;
import org.acnstudents.backend.repository.SlotRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    //Weitere Folgen
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private SlotService slotService;
    @Autowired
    private SlotRepo slotRepo;
    @Autowired
    private CustomerRepo customerRepo;

    public void saveOrUpdate(Order order) {
        orderRepo.save(order);
    }

    public List<org.acnstudents.backend.modelrest.Order> getAllOrders() { // todo: refactor or delete
        List<org.acnstudents.backend.modelrest.Order> newOrders = new ArrayList<>();
        for (Order o : orderRepo.findAll()) {
            org.acnstudents.backend.modelrest.Order newOrder = new org.acnstudents.backend.modelrest.Order();
            newOrder.setComment(o.getComment());
            newOrder.setCustomerid(o.getCustomer().getId());
            newOrder.setCustomer(customerService.mapCustomerToRestCustomer(o.getCustomer()));
            newOrder.setSlotID(o.getSlot().getId());
            newOrder.setTimeslot(slotService.map(o.getSlot()));
            newOrders.add(newOrder);
        }
        return newOrders;
    }

    public int ordersPost(org.acnstudents.backend.modelrest.Order order) {
        Order newOrder = new Order();
        newOrder.setCustomer(customerRepo.findOne(order.getCustomerid()));
        newOrder.setSlot(slotRepo.findOne(order.getSlotID()));
        newOrder.setComment(order.getComment());
        orderRepo.save(newOrder);
        return newOrder.getId();
    }


}
