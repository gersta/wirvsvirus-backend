package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Site;
import org.acnstudents.backend.repository.SiteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
    Only for Backend
*/

@Service
public class BackendService {

    @Autowired
    private SiteRepo siteRepo;

    public Site findById(Integer id) {
        return siteRepo.findOne(id);
    }
}
