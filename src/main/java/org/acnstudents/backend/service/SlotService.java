package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Site;
import org.acnstudents.backend.model.Slot;
import org.acnstudents.backend.modelrest.Timeslot;
import org.acnstudents.backend.repository.OrderRepo;
import org.acnstudents.backend.repository.SlotRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.threeten.bp.Instant;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Service
public class SlotService {

    //Weitere Folgen
    @Autowired
    private SlotRepo slotRepo;
    @Autowired
    private SiteService siteService;

    @Autowired
    private BackendService backendService;

    @Autowired
    private OrderRepo orderRepo;

    public Integer saveOrUpdate(Timeslot timeslot, Integer siteId) {
        Site site = backendService.findById(siteId);
        Slot slot = map(timeslot);
        slot.setSite(site);
        slotRepo.save(slot);
        return slot.getId();
    }

    public Timeslot getSlotById(Integer id) {
        return map(slotRepo.findOne(id));
    }

    public List<Timeslot> getSlots(Integer siteId, Integer maxResults, OffsetDateTime before, OffsetDateTime after) {
        Site site = backendService.findById(siteId);
        List<Timeslot> timeslots = new ArrayList<>();
        List<Slot> slots = slotRepo.findAllByStartAfterAndSiteOrderByStartAsc(new Timestamp(System.currentTimeMillis()), site);
        for (Slot slot : slots) {
            timeslots.add(map(slot));
        }
        return timeslots;
    }

    public OffsetDateTime convert(Timestamp timestamp) {
        return OffsetDateTime.ofInstant(Instant.ofEpochMilli(timestamp.getTime()), ZoneId.of("Europe/Berlin"));
    }

    public Timestamp convert(OffsetDateTime offsetDateTime) {
        return new Timestamp(offsetDateTime.atZoneSameInstant(ZoneOffset.of("Europe/Berlin")).toLocalDateTime().toEpochSecond(ZoneOffset.of("Europe/Berlin")) * 1000);
    }

    public Timeslot map(Slot slot) {
        return new Timeslot(
                slot.getSite().getId(),
                slot.getMaxPersons(),
                getCountBySlotId(slot.getId()),
                convert(slot.getStart()),
                convert(slot.getEnd())
        );
    }

    public void saveOrUpdate(Slot slot) {
        slotRepo.save(slot);
    }

    public org.acnstudents.backend.modelrest.Timeslot getTimeslotById(int id) {
        Slot slot = slotRepo.findOne(id);

        if (slot != null) {
            org.acnstudents.backend.modelrest.Timeslot restSlot = new org.acnstudents.backend.modelrest.Timeslot();

            restSlot.setEndTime(OffsetDateTime.ofInstant(Instant.ofEpochMilli(slot.getEnd().getTime()), ZoneId.of("Europe/Berlin")));
            restSlot.setMaximumPersons(slot.getMaxPersons());
            restSlot.setSiteID(slot.getSite().getId());
            restSlot.setStartTime(OffsetDateTime.ofInstant(Instant.ofEpochMilli(slot.getStart().getTime()), ZoneId.of("Europe/Berlin")));

            return restSlot;
        }
        return null;
    }

    public int getCountBySlotId(int id) {
        return orderRepo.countBySlotId(id);
    }

    public Slot map(Timeslot slot) {
        return new Slot(
                slot.getMaximumPersons(),
                convert(slot.getStartTime()),
                convert(slot.getEndTime())
        );
    }


}
