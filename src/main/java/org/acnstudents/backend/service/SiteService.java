package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Site;
import org.acnstudents.backend.repository.SiteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SiteService {

    //Weitere Folgen
    @Autowired
    private SiteRepo siteRepo;

    public int saveOrUpdate(org.acnstudents.backend.modelrest.Site site) {
        Site respSite = siteRepo.save(mapJSONToJava(site));
        return respSite.getId();
    }

    public org.acnstudents.backend.modelrest.Site findById(Integer id) {
        Site respSite = siteRepo.findOne(id);
        return map(respSite);
    }


    public List<org.acnstudents.backend.modelrest.Site> sitesGet(String zip) {
        if (zip != null) {
            return map(siteRepo.findAllByZipCodeStartsWith(zip));
        }
        return map(siteRepo.findAll());
    }

    public List<org.acnstudents.backend.modelrest.Site> map(List<Site> sites) {
        List<org.acnstudents.backend.modelrest.Site> result = new ArrayList<>();
        for (Site site : sites) {
            result.add(map(site));
        }
        return result;
    }

    public org.acnstudents.backend.modelrest.Site map(Site respSite) {
        return new org.acnstudents.backend.modelrest.Site(
                respSite.getId(),
                respSite.getName(),
                respSite.getAddress(),
                respSite.getZipCode(),
                respSite.getHomepage(),
                1,
                1
        );
    }

    private Site mapJSONToJava(org.acnstudents.backend.modelrest.Site respSite) {
        return new Site(
                respSite.getName(),
                respSite.getAddress(),
                respSite.getHomepage(),
                respSite.getZipCode()
        );
    }

    public List<Site> findAllByZipCodeStartsWith(String zipCode) {
        return siteRepo.findAllByZipCodeStartsWith(zipCode);
    }
}
