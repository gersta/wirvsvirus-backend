package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Customer;
import org.acnstudents.backend.repository.CustomerRepo;
import org.acnstudents.backend.repository.SiteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class CustomerService {

    //Weitere Folgen
    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private SiteRepo siteRepo;

    public void saveOrUpdate(Customer customer) {
        customerRepo.save(customer);
    }

    public int registerCustomer(org.acnstudents.backend.modelrest.Customer customer) {
        if (!emailExists(customer.getEmail())) {
            Customer newCustomer = mapCustomer(customer);
            customerRepo.save(newCustomer);
            return newCustomer.getId();
        }
        return -1;

    }


    private boolean emailExists(String email) {
        return getCustomerByEmail(email) != null;
    }


    public org.acnstudents.backend.modelrest.Customer getCustomerById(int id) {
        Customer customer = customerRepo.findOne(id);

        if (customer != null) {
            org.acnstudents.backend.modelrest.Customer restCustomer = new org.acnstudents.backend.modelrest.Customer();
            restCustomer.setFirstName(customer.getFirstName());
            restCustomer.setLastName(customer.getLastName());
            restCustomer.setEmail(customer.getEmail());
            restCustomer.setPassword("");
            restCustomer.setSiteId(customer.getSite().getId());
            restCustomer.setIsVerified(customer.isVerified());
            restCustomer.setEmailConfirmed(customer.isEmailConfirmed());
            restCustomer.setNumberAdults(customer.getNumberAdults() + customer.getNumberChilds());
            restCustomer.setNumberChilds(0);
            restCustomer.setIsSiteManager(customer.isSiteManager());
            restCustomer.setZipCode(customer.getZipCode());
            restCustomer.setPreferences(Arrays.asList(customer.getPreferences().split(";")));

            return restCustomer;
        }
        return null;
    }

    public Customer getCustomerByEmail(String email) {
        return customerRepo.findOneByEmail(email);
    }


    public Customer mapCustomer(org.acnstudents.backend.modelrest.Customer customer) {
        Customer newCustomer = new Customer();
        newCustomer.setFirstName(customer.getFirstName());
        newCustomer.setLastName(customer.getLastName());
        newCustomer.setEmail(customer.getEmail());

        if(customer.getSiteId() != null) {
            newCustomer.setSite(siteRepo.findOne(customer.getSiteId()));
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
        newCustomer.setPassword(encoder.encode(customer.getPassword()));
        newCustomer.setIsVerified(false);
        newCustomer.setEmailConfirmed(false);
        newCustomer.setNumberAdults(customer.getNumberAdults());
        newCustomer.setNumberChilds(customer.getNumberChilds());
        newCustomer.setSiteManager(false);
        newCustomer.setZipCode(customer.getZipCode());

        StringBuilder stringBuilder = new StringBuilder();
        for (String str : customer.getPreferences())
            stringBuilder.append(str);
        newCustomer.setPreferences(stringBuilder.toString());
        return newCustomer;
    }

    public org.acnstudents.backend.modelrest.Customer mapCustomerToRestCustomer(Customer customer) {
        org.acnstudents.backend.modelrest.Customer newCustomer = new org.acnstudents.backend.modelrest.Customer();
        newCustomer.setFirstName(customer.getFirstName());
        newCustomer.setLastName(customer.getLastName());
        newCustomer.setEmail(customer.getEmail());
        newCustomer.setSiteId(customer.getSite() == null ? null : customer.getSite().getId());
        newCustomer.setIsVerified(false);
        newCustomer.setEmailConfirmed(false);
        newCustomer.setNumberAdults(customer.getNumberAdults());
        newCustomer.setNumberChilds(customer.getNumberChilds());
        newCustomer.setIsSiteManager(false);
        newCustomer.setPreferences(Arrays.asList(new String[]{customer.getPreferences()}));
        newCustomer.setZipCode(customer.getZipCode());
        return newCustomer;
    }
}
