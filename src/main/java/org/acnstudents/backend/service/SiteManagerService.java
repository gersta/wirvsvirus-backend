package org.acnstudents.backend.service;

import org.acnstudents.backend.model.SiteManager;
import org.acnstudents.backend.repository.SiteManagerRepo;
import org.acnstudents.backend.repository.SiteRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SiteManagerService {
    //Weitere Folgen
    @Autowired
    private SiteManagerRepo siteManagerRepo;

    @Autowired
    private SiteRepo siteRepo;

    public void saveOrUpdate(SiteManager lead) {
        siteManagerRepo.save(lead);
    }

    public int registerSiteManager(org.acnstudents.backend.modelrest.SiteManager siteManager, int siteId) {
        if (isEmailAvailable(siteManager.getEmail())) {
            SiteManager newSiteManager = new SiteManager();
            newSiteManager.setFirstName(siteManager.getFirstName());
            newSiteManager.setLastName(siteManager.getLastName());
            newSiteManager.setEmail(siteManager.getEmail());
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
            newSiteManager.setPassword(encoder.encode(siteManager.getPassword()));
            newSiteManager.setIsVerified(false);
            newSiteManager.setEmailConfirmed(false);
            newSiteManager.setSiteManager(true);
            newSiteManager.setPhoneNumber(siteManager.getPhoneNumber());
            newSiteManager.setSite(siteRepo.findOne(siteId));
            siteManagerRepo.save(newSiteManager);
            return newSiteManager.getId();
        }
        return -1;
    }

    private boolean isEmailAvailable(String email) {
        return siteManagerRepo.findOneByEmail(email) == null;
    }

}
