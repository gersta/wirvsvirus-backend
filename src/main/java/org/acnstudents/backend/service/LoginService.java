package org.acnstudents.backend.service;

import org.acnstudents.backend.model.Customer;
import org.acnstudents.backend.model.SiteManager;
import org.acnstudents.backend.model.User;
import org.acnstudents.backend.repository.CustomerRepo;
import org.acnstudents.backend.repository.SiteManagerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class LoginService {

    //Weitere Folgen
    @Autowired
    private org.acnstudents.backend.repository.UserRepo userRepo;

    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private SiteManagerRepo siteManagerRepo;


    public org.acnstudents.backend.modelrest.User login(org.acnstudents.backend.modelrest.Body1 body) {
        User user = getUserByEMail(body.getEmail());
        if (user == null)
            return null;
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
        if (encoder.matches(body.getPassword(), user.getPassword())) {
            org.acnstudents.backend.modelrest.User result = new org.acnstudents.backend.modelrest.User();
            if (user.isSiteManager()) {
                SiteManager siteManager = siteManagerRepo.findOne(user.getId());
                result.siteId(siteManager.getSite().getId());
                result.phone(siteManager.getPhoneNumber());
            } else {
                Customer customer = customerRepo.findOne(user.getId());
                result.siteId(customer.getSite() == null ? null : customer.getSite().getId());
            }
            result.email(user.getEmail())
                    .lastName(user.getLastName())
                    .isVerified(user.isVerified())
                    .isSiteManager(user.isSiteManager())
                    .firstName(user.getFirstName());
            return result;
        }
        return null;
    }

    private User getUserByEMail(String email) {
        return userRepo.findOneByEmail(email);
    }

}
