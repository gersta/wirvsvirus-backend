/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.18).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package org.acnstudents.backend.api;

import io.swagger.annotations.*;
import org.acnstudents.backend.modelrest.Body1;
import org.acnstudents.backend.modelrest.Error500;
import org.acnstudents.backend.modelrest.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
@Api(value = "login", description = "the login API")
public interface LoginApi {

    @ApiOperation(value = "Login for user.", nickname = "loginPost", notes = "", tags = {"user",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Logged in", response = User.class),
            @ApiResponse(code = 403, message = "FORBIDDEN"),
            @ApiResponse(code = 500, message = "SERVER ERROR", response = Error500.class)})
    @RequestMapping(value = "/login",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<User> loginPost(@ApiParam(value = "", required = true) @Valid @RequestBody Body1 body
    );

}
