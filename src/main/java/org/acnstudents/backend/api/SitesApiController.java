package org.acnstudents.backend.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.acnstudents.backend.modelrest.Body;
import org.acnstudents.backend.modelrest.InlineResponse201;
import org.acnstudents.backend.modelrest.Site;
import org.acnstudents.backend.modelrest.Timeslot;
import org.acnstudents.backend.service.CombinedService;
import org.acnstudents.backend.service.SiteService;
import org.acnstudents.backend.service.SlotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.threeten.bp.OffsetDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
@RestController
@Controller
public final class SitesApiController implements SitesApi {

    private static final Logger log = LoggerFactory.getLogger(SitesApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private SiteService siteService;

    @Autowired
    private SlotService slotService;

    @Autowired
    private CombinedService combinedService;

    @org.springframework.beans.factory.annotation.Autowired
    public SitesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public List<Site> sitesGet(@ApiParam(value = "") @RequestParam(value = "zipCode", required = false) String zipCode
    ) {
        return siteService.sitesGet(zipCode);
    }

    public ResponseEntity<InlineResponse201> sitesPost(@ApiParam(value = "", required = true) @Valid @RequestBody Body body
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<InlineResponse201>(combinedService.saveSiteAndSiteManager(body), HttpStatus.CREATED);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<InlineResponse201>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<InlineResponse201>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Object>> sitesSiteIDOrdersGet(@ApiParam(value = "", required = true) @PathVariable("siteId") Integer siteId
            , @ApiParam(value = "") @Valid @RequestParam(value = "maxResults", required = false) Integer maxResults
            , @ApiParam(value = "") @Valid @RequestParam(value = "before", required = false) OffsetDateTime before
            , @ApiParam(value = "") @Valid @RequestParam(value = "after", required = false) OffsetDateTime after
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Object>>(objectMapper.readValue("[ \"\", \"\" ]", List.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Object>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Timeslot>> sitesSiteIDTimeslotsGet(@ApiParam(value = "", required = true) @PathVariable("siteId") Integer siteId
            , @ApiParam(value = "") @Valid @RequestParam(value = "maxResults", required = false) Integer maxResults
            , @ApiParam(value = "") @Valid @RequestParam(value = "before", required = false) OffsetDateTime before
            , @ApiParam(value = "") @Valid @RequestParam(value = "after", required = false) OffsetDateTime after
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Timeslot>>(slotService.getSlots(siteId, maxResults, before, after), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Timeslot>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Timeslot>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Integer> sitesSiteIDTimeslotsPost(@ApiParam(value = "", required = true) @Valid @PathVariable("siteId") Integer siteId, @RequestBody Timeslot body
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Integer>(slotService.saveOrUpdate(body, siteId), HttpStatus.CREATED);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<Integer>(HttpStatus.NOT_IMPLEMENTED);
    }
}
