package org.acnstudents.backend.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.acnstudents.backend.modelrest.Customer;
import org.acnstudents.backend.modelrest.SiteManager;
import org.acnstudents.backend.service.CustomerService;
import org.acnstudents.backend.service.SiteManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.threeten.bp.OffsetDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SiteManagerService siteManagerService;

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Integer> usersCustomersPost(@ApiParam(value = "", required = true) @Valid @RequestBody Customer body
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                int response = customerService.registerCustomer(body);
                if(response == -1){
                    return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
                }
                return new ResponseEntity<Integer>(response,HttpStatus.CREATED);

            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Integer>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Integer> usersSiteManagersPost(@ApiParam(value = "", required = true) @Valid @RequestBody SiteManager body
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                int response = siteManagerService.registerSiteManager(body, 0); // todo: remove whole method
                if(response == -1){
                    return new ResponseEntity<Integer>(HttpStatus.FORBIDDEN);
                }
                return new ResponseEntity<Integer>(response,HttpStatus.CREATED);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Integer>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<Object>> usersUserIDOrdersGet(@ApiParam(value = "", required = true) @PathVariable("userId") Integer userId
            , @ApiParam(value = "") @Valid @RequestParam(value = "maxResults", required = false) Integer maxResults
            , @ApiParam(value = "") @Valid @RequestParam(value = "before", required = false) OffsetDateTime before
            , @ApiParam(value = "") @Valid @RequestParam(value = "after", required = false) OffsetDateTime after
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<Object>>(objectMapper.readValue("[ \"\", \"\" ]", List.class), HttpStatus.OK);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<Object>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
