package org.acnstudents.backend;

import org.acnstudents.backend.model.*;
import org.acnstudents.backend.repository.*;
import org.acnstudents.backend.service.OrderService;
import org.acnstudents.backend.service.SlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Transactional
@Component
public class Setup implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private SlotService slotService;

    @Autowired
    private UserRepo repo;

    @Autowired
    private SlotRepo slotRepo;

    @Autowired
    private CustomerRepo customerRepo;

    @Autowired
    private OrderRepo orderRepo;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SiteRepo siteRepo;

    @Autowired
    private SiteManagerRepo siteManagerRepo;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //Users todo: PW hashing
        User admin = new User();
        admin.setEmail("admin@tafel.de");
        admin.setPassword("admin");
        admin.setEnabled(true);
        repo.save(admin);

        //Users todo: PW hashing
        SiteManager luisa = new SiteManager();
        luisa.setEmail("luisa@tafel.de");
        luisa.setPassword("luisa");
        luisa.setEnabled(true);
        luisa.setPhoneNumber("07112531648");
        luisa.setFirstName("Luisa");
        luisa.setLastName("Schmidt");
        luisa.setIsVerified(true);
        luisa.setEmailConfirmed(true);
        repo.save(luisa);

        //Users todo: PW hashing
        //User karl = new User("karl@mail.de", "karl", "karl123", true, true);
        //repo.save(karl);

        Set<Slot> slotSet = new HashSet<>();
        Long offset = Timestamp.valueOf("2020-04-01 08:00:00").getTime();
        final long SLOTLENGHT = 30L * 60L * 1000L;

        //SiteManager
        SiteManager franzi = new SiteManager("franzi@tafel.de", "franzi", "Franzi", "Loh", true, true, true, "01724588789");
        siteManagerRepo.save(franzi);

        //SiteManager
        SiteManager emil = new SiteManager("emil@tafel.de", "emil", "Emil", "Wonka", true, true, true, "01724588789");
        siteManagerRepo.save(emil);

        //SiteManager
        SiteManager moritz = new SiteManager("moritz@tafel.de", "moritz", "Moritz", "Jenda", true, true, true, "01724588789");
        siteManagerRepo.save(moritz);

        //SiteManager
        SiteManager thomas = new SiteManager("thomas@tafel.de", "thomas", "Thomas", "Merels", true, true, true, "0176578988789");
        siteManagerRepo.save(thomas);

        //SiteManager
        SiteManager angelo = new SiteManager("angelo@tafel.de", "angelo", "Angelo", "Martinez", true, true, true, "017234786324");
        siteManagerRepo.save(angelo);

        Site site1 = new Site("Abensberger Tafel e.V.", "Bad Gögginger Weg 22, 93326 Abensberg", "http://www.abensberg-tafel.de", "86553");
        site1.setSiteManager(franzi);
        //site1.setSlots(slotList);
        siteRepo.save(site1);


        Site site2 = new Site("Ahauser Tafel", "Schloßstraße 16, 48683 Ahaus", "www.aachener-tafel.de", "48683");
        site2.setSiteManager(luisa);
        siteRepo.save(site2);

        Site site3 = new Site("Bad Sachsaer Tafel e.V.", "Kirchstraße 20, 37441 Bad Sachsa", "www.alfelder-tafel.de", "37441");
        site3.setSiteManager(emil);
        siteRepo.save(site3);

        Site site4 = new Site("Bad Schwartauer Tafel", "Mühlenstr. 27, 23611 Bad Schwartau", "www.badbramstedtertafel.de", "23611");
        site4.setSiteManager(thomas);
        siteRepo.save(site4);

        Site site5 = new Site("Kaller Tafel e.V.", "Aachener Str. 51, 53925 Kall", "www.kaller-tafel.de", "Kall");
        site5.setSiteManager(angelo);
        siteRepo.save(site5);

        Site site6 = new Site("Freiberger Tafel", "Friedeburgerstr. 19, 09599 Freiberg", "www.drk-dippoldiswalde.de", "09599");
        site6.setSiteManager(moritz);
        siteRepo.save(site6);


        //Customer
        Customer karlCustomer = new Customer("karl@mail.de", "karl123", "Karl", "Binder", false, true, true, 0, 1, "Keine Nüsse", "93325");
        karlCustomer.setSite(site1);
        customerRepo.save(karlCustomer);

        //Customer
        Customer olafCustomer = new Customer("Olaf@mail.de", "karl123", "Olaf", "Maier", false, true, true, 0, 1, "Keine Nüsse", "93325");
        olafCustomer.setSite(site2);
        customerRepo.save(olafCustomer);
        //Customer
        Customer simoneCustomer = new Customer("Simone@mail.de", "karl123", "Simone", "Mueller", false, true, true, 0, 1, "Keine Nüsse", "93325");
        simoneCustomer.setSite(site1);
        customerRepo.save(simoneCustomer);
        //Customer
        Customer mikeCustomer = new Customer("Mike@mail.de", "karl123", "Mike", "Herbert", false, true, true, 0, 1, "Keine Nüsse", "93325");
        mikeCustomer.setSite(site3);
        customerRepo.save(mikeCustomer);

        //Slot
        Slot karlSlot = new Slot(3, new Timestamp(offset), new Timestamp(offset + SLOTLENGHT));
        karlSlot.setSite(site1);
        slotRepo.save(karlSlot);
        //Order
        Order karlOrder = new Order("Vegetables");
        karlOrder.setSlot(karlSlot);
        karlOrder.setCustomer(karlCustomer);
        orderRepo.save(karlOrder);


        for (int i = 0; i < 20; i++) {
            offset += SLOTLENGHT;
            Slot slot = new Slot(3, new Timestamp(offset), new Timestamp(offset + SLOTLENGHT));
            Slot slot2 = new Slot(3, new Timestamp(offset), new Timestamp(offset + SLOTLENGHT));
            slot.setSite(site1);
            slot2.setSite(site2);
            slotSet.add(slot);
            slotRepo.save(slot);
            slotRepo.save(slot2);
        }

        for (int j = 2; j < 8; j++) {
            offset = Timestamp.valueOf("2020-04-0" + j + " 08:00:00").getTime();

            for (int i = 0; i < 20; i++) {
                offset += SLOTLENGHT;
                Slot slot = new Slot(3, new Timestamp(offset), new Timestamp(offset + SLOTLENGHT));
                Slot slot2 = new Slot(3, new Timestamp(offset), new Timestamp(offset + SLOTLENGHT));
                slot.setSite(site1);
                slot2.setSite(site2);
                slotSet.add(slot);
                slotRepo.save(slot);
                slotRepo.save(slot2);
            }
        }

        Order order;
        //3 times 66
        order = new Order(customerRepo.findOne(9), slotRepo.findOne(66), "Keine Äpfel");
        orderRepo.save(order);
        Order order2 = new Order(customerRepo.findOne(8), slotRepo.findOne(66), "Keine Birne");
        orderRepo.save(order2);
        order = new Order(customerRepo.findOne(10), slotRepo.findOne(66), "Kein Brot");
        orderRepo.save(order);
        //3 times 68
        order = new Order(customerRepo.findOne(9), slotRepo.findOne(68), "Keine Butter");
        orderRepo.save(order);
        order = new Order(customerRepo.findOne(10), slotRepo.findOne(68), "Bitte Salz");
        orderRepo.save(order);
        order = new Order(customerRepo.findOne(8), slotRepo.findOne(68), "Bitte mit Sahne");
        orderRepo.save(order);
        //2 times 67
        order = new Order(customerRepo.findOne(11), slotRepo.findOne(67), "Süßigkeiten");
        orderRepo.save(order);
        order = new Order(customerRepo.findOne(8), slotRepo.findOne(67), "Bier");
        orderRepo.save(order);
        //single 70
        order = new Order(customerRepo.findOne(9), slotRepo.findOne(70), "keine Milchprodukte");
        orderRepo.save(order);









    }


}
