package org.acnstudents.backend.model;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "Slot")
public class Slot {

    @Id
    @Column(name = "timeslot_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany
    Set<Order> orders;

    @Column(name = "max_persons")
    private int maxPersons;

    @Column(name = "date_end")
    private Timestamp end;

    @Column(name = "date_start")
    private Timestamp start;

    @OneToMany(mappedBy = "slot")
    private Set <Order> order;

    @ManyToOne
    @JoinColumn(name = "site_id", nullable = false)
    private Site site;

    public Slot() {
    }

    public Slot(int maxPersons, Timestamp start, Timestamp end) {
        this.maxPersons = maxPersons;
        this.start = start;
        this.end = end;
    }

    public Slot(Integer id, int maxPersons, Timestamp end, Timestamp start) {
        this.id = id;
        this.maxPersons = maxPersons;
        this.end = end;
        this.start = start;
    }


    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    public int getMaxPersons() {
        return maxPersons;
    }

    public void setMaxPersons(int maxPersons) {
        this.maxPersons = maxPersons;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Set<Order> getOrder() {
        return order;
    }

    public void setOrder(Set<Order> order) {
        this.order = order;
    }
}
