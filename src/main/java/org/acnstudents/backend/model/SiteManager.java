package org.acnstudents.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "site_manager")
public class SiteManager extends User {

    @Column(name = "phone_number")
    private String phoneNumber;

    @JoinColumn
    @OneToOne(cascade = CascadeType.ALL)
    private Site site;

    public SiteManager() {
    }

    public SiteManager(String email, String password, String firstName, String lastName, boolean isSiteManager, boolean isVerified, boolean emailConfirmed, String phoneNumber) {
        super(email, password, firstName, lastName, isSiteManager, isVerified, emailConfirmed);
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
}