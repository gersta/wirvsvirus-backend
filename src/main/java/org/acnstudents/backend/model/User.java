package org.acnstudents.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Integer id;

    @Column(unique = true)
    private String email="";

    @Column
    private String password;

    @Column(name = "email_confirmed")
    private boolean emailConfirmed;

    @NotNull
    @Column(name = "is_site_manager")
    private boolean isSiteManager;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    @JsonIgnore
    private boolean isVerified;

    private boolean enabled;

    public User() {
    }

    public User(String email, String password, String firstName, String lastName, boolean isSiteManager, boolean isVerified, boolean emailConfirmed) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isSiteManager = isSiteManager;
        this.isVerified = isVerified;
        this.emailConfirmed = emailConfirmed;
        this.enabled = false;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isEmailConfirmed() {
        return emailConfirmed;
    }

    public void setEmailConfirmed(boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public boolean isSiteManager() {
        return isSiteManager;
    }

    public void setSiteManager(boolean siteManager) {
        isSiteManager = siteManager;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setIsVerified(boolean isVerified) {
        this.isVerified = isVerified;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
