package org.acnstudents.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "food_order")
public class Order {

    @Id
    @Column(name = "order_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "timeslot_id", unique = false)
    private Slot slot;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", unique = false)
    private Customer customer;

    @Column(name = "comment")
    private String comment;


    public Order() {
    }

    public Order(Slot slot, String comment){
        this.slot = slot;
        this.comment = comment;
    }

    public Order(Customer customer, Slot slot, String comment) {
        this.customer = customer;
        this.slot = slot;
        this.comment = comment;
    }

    public Order(String comment) {
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Slot getSlot() {
        return slot;
    }

    public void setSlot(Slot slot) {
        this.slot = slot;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
