package org.acnstudents.backend.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Site")
public class Site {

    @OneToMany(mappedBy = "site")
    @JsonIgnore
    Set<Slot> slots;

    @Column(name = "geo_location")
    @JsonIgnore
    public String zipCode;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "homepage")
    private String homepage;
    @Id
    @Column(name = "site_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    @OneToOne(mappedBy = "site")
    @JsonIgnore
    private SiteManager siteManager;

    @OneToMany(mappedBy = "site")
    @JsonIgnore
    private Set<Customer> customersSet;

    public Site() {
    }

    public Site(String name, String address, String homepage, String zipCode) {
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.homepage = homepage;
    }

    public Set<Slot> getSlots() {
        return slots;
    }

    public void setSlots(Set<Slot> slots) {
        this.slots = slots;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public SiteManager getSiteManager() {
        return siteManager;
    }

    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }
}