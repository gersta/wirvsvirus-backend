package org.acnstudents.backend.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Customer")
public class Customer extends User {

    @OneToMany
    Set<Order> orders;

    @Column(name = "number_childs")
    private int numberChilds;

    @Column(name = "number_adults")
    private int numberAdults;

    @Column(name = "preferences")
    private String preferences;

    @Column(name = "zipCode")
    private String zipCode;

    @OneToMany(mappedBy = "customer")
    private Set<Order> order;

    @ManyToOne
    @JoinColumn(name = "site_id")
    private Site site;

    public Customer() {
    }

    public Customer(String email, String password, String firstName, String lastName, boolean isSiteManager, boolean isVerified, boolean emailConfirmed, int numberChilds, int numberAdults, String preferences, String zipCode) {
        super(email, password, firstName, lastName, isSiteManager, isVerified, emailConfirmed);
        this.numberAdults = numberAdults;
        this.numberChilds = numberChilds;
        this.preferences = preferences;
        this.zipCode = zipCode;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Set<Order> getOrder() {
        return order;
    }

    public void setOrder(Set<Order> order) {
        this.order = order;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    public int getNumberChilds() {
        return numberChilds;
    }

    public void setNumberChilds(int numberChilds) {
        this.numberChilds = numberChilds;
    }

    public int getNumberAdults() {
        return numberAdults;
    }

    public void setNumberAdults(int numberAdults) {
        this.numberAdults = numberAdults;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }
}
