package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import org.threeten.bp.OffsetDateTime;

import javax.validation.Valid;
import java.util.Objects;

/**
 * Error500
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Error500   {
  @JsonProperty("statusCode")
  private Integer statusCode = null;

  @JsonProperty("logErrorId")
  private String logErrorId = null;

  @JsonProperty("requestUrl")
  private String requestUrl = null;

  @JsonProperty("requestTimestamp")
  private OffsetDateTime requestTimestamp = null;

  public Error500 statusCode(Integer statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  /**
   * Get statusCode
   * @return statusCode
  **/
  @ApiModelProperty(example = "500", value = "")
  
    public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public Error500 logErrorId(String logErrorId) {
    this.logErrorId = logErrorId;
    return this;
  }

  /**
   * Get logErrorId
   * @return logErrorId
  **/
  @ApiModelProperty(example = "MRP1000", value = "")
  
    public String getLogErrorId() {
    return logErrorId;
  }

  public void setLogErrorId(String logErrorId) {
    this.logErrorId = logErrorId;
  }

  public Error500 requestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
    return this;
  }

  /**
   * Get requestUrl
   * @return requestUrl
  **/
  @ApiModelProperty(example = "https://www.frankfurter-tafel.de/", value = "")
  
    public String getRequestUrl() {
    return requestUrl;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

  public Error500 requestTimestamp(OffsetDateTime requestTimestamp) {
    this.requestTimestamp = requestTimestamp;
    return this;
  }

  /**
   * Get requestTimestamp
   * @return requestTimestamp
  **/
  @ApiModelProperty(value = "")
  
    @Valid
    public OffsetDateTime getRequestTimestamp() {
    return requestTimestamp;
  }

  public void setRequestTimestamp(OffsetDateTime requestTimestamp) {
    this.requestTimestamp = requestTimestamp;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error500 error500 = (Error500) o;
    return Objects.equals(this.statusCode, error500.statusCode) &&
        Objects.equals(this.logErrorId, error500.logErrorId) &&
        Objects.equals(this.requestUrl, error500.requestUrl) &&
        Objects.equals(this.requestTimestamp, error500.requestTimestamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusCode, logErrorId, requestUrl, requestTimestamp);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error500 {\n");
    
    sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
    sb.append("    logErrorId: ").append(toIndentedString(logErrorId)).append("\n");
    sb.append("    requestUrl: ").append(toIndentedString(requestUrl)).append("\n");
    sb.append("    requestTimestamp: ").append(toIndentedString(requestTimestamp)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
