package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * User
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class User {
  @JsonProperty("siteId")
  private Integer siteId = null;

  @JsonProperty("firstName")
  private String firstName = null;

  @JsonProperty("lastName")
  private String lastName = null;

  @JsonProperty("email")
  private String email = null;

  @JsonProperty("phone")
  private String phone = null;

  @JsonProperty("password")
  private String password = null;

  @JsonProperty("isVerified")
  private Boolean isVerified = null;

  @JsonProperty("emailConfirmed")
  private Boolean emailConfirmed = null;

  @JsonProperty("isSiteManager")
  private Boolean isSiteManager = null;

  public User firstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  /**
   * Get firstName
   *
   * @return firstName
   **/
  @ApiModelProperty(example = "Max", value = "")

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public User lastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  /**
   * Get lastName
   *
   * @return lastName
   **/
  @ApiModelProperty(example = "Mustermann", value = "")

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public User email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "max.mustermann@mustermail.de", value = "")
  
    public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public User password(String password) {
    this.password = password;
    return this;
  }

  /**
   * Get password
   * @return password
   **/
  @ApiModelProperty(example = "***", value = "")

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public User isVerified(Boolean isVerified) {
    this.isVerified = isVerified;
    return this;
  }

  /**
   * Get isVerified
   *
   * @return isVerified
   **/
  @ApiModelProperty(example = "true", value = "")

  public Boolean isVerified() {
    return isVerified;
  }

  public void setIsVerified(Boolean isVerified) {
    this.isVerified = isVerified;
  }

  public User emailConfirmed(Boolean emailConfirmed) {
    this.emailConfirmed = emailConfirmed;
    return this;
  }

  /**
   * Get emailConfirmed
   * @return emailConfirmed
   **/
  @ApiModelProperty(example = "true", value = "")

  public Boolean isEmailConfirmed() {
    return emailConfirmed;
  }

  public void setEmailConfirmed(Boolean emailConfirmed) {
    this.emailConfirmed = emailConfirmed;
  }

  public User isSiteManager(Boolean isSiteManager) {
    this.isSiteManager = isSiteManager;
    return this;
  }

  /**
   * Get isSiteManager
   *
   * @return isSiteManager
   **/
  @ApiModelProperty(example = "false", value = "")

  public Boolean isIsSiteManager() {
    return isSiteManager;
  }

  public void setIsSiteManager(Boolean isSiteManager) {
    this.isSiteManager = isSiteManager;
  }

  public User siteId(Integer siteId) {
    this.siteId = siteId;
    return this;
  }

  /**
   * Get siteId
   *
   * @return siteId
   **/
  public Integer getSiteId() {
    return siteId;
  }

  public void setSiteId(Integer siteId) {
    this.siteId = siteId;
  }

  public User phone(String phone) {
    this.phone = phone;
    return this;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return Objects.equals(siteId, user.siteId) &&
            Objects.equals(firstName, user.firstName) &&
            Objects.equals(lastName, user.lastName) &&
            Objects.equals(email, user.email) &&
            Objects.equals(phone, user.phone) &&
            Objects.equals(password, user.password) &&
            Objects.equals(isVerified, user.isVerified) &&
            Objects.equals(emailConfirmed, user.emailConfirmed) &&
            Objects.equals(isSiteManager, user.isSiteManager);
  }

  @Override
  public int hashCode() {
    return Objects.hash(siteId, firstName, lastName, email, phone, password, isVerified, emailConfirmed, isSiteManager);
  }

  @Override
  public String toString() {
    return "User{" +
            "siteId=" + siteId +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", password='" + password + '\'' +
            ", isVerified=" + isVerified +
            ", emailConfirmed=" + emailConfirmed +
            ", isSiteManager=" + isSiteManager +
            '}';
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
