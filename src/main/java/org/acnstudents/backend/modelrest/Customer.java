package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Customer
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Customer extends User {

  @JsonProperty("numberChilds")
  private Integer numberChilds = null;

  @JsonProperty("numberAdults")
  private Integer numberAdults = null;

  @JsonProperty("zipCode")
  private String zipCode = null;

  @JsonProperty("preferences")
  @Valid
  private List<String> preferences = null;

  public Customer numberChilds(Integer numberChilds) {
    this.numberChilds = numberChilds;
    return this;
  }

  /**
   * Get numberChilds
   *
   * @return numberChilds
   **/
  @ApiModelProperty(example = "2", value = "")

  public Integer getNumberChilds() {
    return numberChilds;
  }

  public void setNumberChilds(Integer numberChilds) {
    this.numberChilds = numberChilds;
  }


  public Customer numberAdults(Integer numberAdults) {
    this.numberAdults = numberAdults;
    return this;
  }

  /**
   * Get numberAdults
   *
   * @return numberAdults
   **/
  @ApiModelProperty(example = "2", value = "")

  public Integer getNumberAdults() {
    return numberAdults;
  }

  public void setNumberAdults(Integer numberAdults) {
    this.numberAdults = numberAdults;
  }

  public Customer zipCode(String zipCode) {
    this.zipCode = zipCode;
    return this;
  }

  /**
   * Get zipCode
   *
   * @return zipCode
   **/
  @ApiModelProperty(example = "88055", value = "")

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public Customer preferences(List<String> preferences) {
    this.preferences = preferences;
    return this;
  }

  public Customer addPreferencesItem(String preferencesItem) {
    if (this.preferences == null) {
      this.preferences = new ArrayList<String>();
    }
    this.preferences.add(preferencesItem);
    return this;
  }

  /**
   * Get preferences
   *
   * @return preferences
   **/
  @ApiModelProperty(value = "")

  public List<String> getPreferences() {
    return preferences;
  }

  public void setPreferences(List<String> preferences) {
    this.preferences = preferences;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Customer customer = (Customer) o;
    return Objects.equals(this.numberChilds, customer.numberChilds) &&
            Objects.equals(this.numberAdults, customer.numberAdults) &&
            Objects.equals(this.zipCode, customer.zipCode) &&
            Objects.equals(this.preferences, customer.preferences) &&
            super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(numberChilds, numberAdults, zipCode, preferences, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Customer {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    numberChilds: ").append(toIndentedString(numberChilds)).append("\n");
    sb.append("    numberAdults: ").append(toIndentedString(numberAdults)).append("\n");
    sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
    sb.append("    preferences: ").append(toIndentedString(preferences)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
