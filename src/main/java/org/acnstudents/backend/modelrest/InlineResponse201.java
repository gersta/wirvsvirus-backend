package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * InlineResponse201
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class InlineResponse201 {
  @JsonProperty("site")
  private Integer site = null;

  @JsonProperty("siteManager")
  private Integer siteManager = null;

  public InlineResponse201 site(Integer site) {
    this.site = site;
    return this;
  }

  /**
   * Get site
   *
   * @return site
   **/
  @ApiModelProperty(value = "")

  public Integer getSite() {
    return site;
  }

  public void setSite(Integer site) {
    this.site = site;
  }

  public InlineResponse201 siteManager(Integer siteManager) {
    this.siteManager = siteManager;
    return this;
  }

  /**
   * Get siteManager
   *
   * @return siteManager
   **/
  @ApiModelProperty(value = "")

  public Integer getSiteManager() {
    return siteManager;
  }

  public void setSiteManager(Integer siteManager) {
    this.siteManager = siteManager;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InlineResponse201 inlineResponse201 = (InlineResponse201) o;
    return Objects.equals(this.site, inlineResponse201.site) &&
            Objects.equals(this.siteManager, inlineResponse201.siteManager);
  }

  @Override
  public int hashCode() {
    return Objects.hash(site, siteManager);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class InlineResponse201 {\n");

    sb.append("    site: ").append(toIndentedString(site)).append("\n");
    sb.append("    siteManager: ").append(toIndentedString(siteManager)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
