package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * Site
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Site {
    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("address")
    private String address = null;

    @JsonProperty("zipCode")
    private String zipCode = null;

    @JsonProperty("homepage")
    private String homepage = null;

    @JsonProperty("repetitions")
    private Integer repetitions = null;

    @JsonProperty("maximumPortions")
    private Integer maximumPortions = null;

  public Site() {
    
  }

    public Site(Integer id, String name, String address, String zipCode, String homepage, Integer repetitions, Integer maximumPortions) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.homepage = homepage;
        this.repetitions = repetitions;
        this.maximumPortions = maximumPortions;
    }

  public Site name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @ApiModelProperty(example = "", value = "")

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "Frankfurter Tafel", value = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Site address(String address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
  **/
  @ApiModelProperty(example = "Campus Kronberg 1", value = "")

  public String getAddress() {
      return address;
  }

    public void setAddress(String address) {
        this.address = address;
    }

    public Site zipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    /**
     * Get zipCode
     *
     * @return zipCode
     **/
    @ApiModelProperty(example = "88055", value = "")

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Site homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    /**
     * Get homepage
     * @return homepage
  **/
  @ApiModelProperty(example = "https://www.frankfurter-tafel.de/", value = "")
  
    public String getHomepage() {
    return homepage;
  }

  public void setHomepage(String homepage) {
    this.homepage = homepage;
  }

  public Site repetitions(Integer repetitions) {
    this.repetitions = repetitions;
    return this;
  }

  /**
   * Get repetitions
   * @return repetitions
  **/
  @ApiModelProperty(example = "3", value = "")
  
    public Integer getRepetitions() {
    return repetitions;
  }

  public void setRepetitions(Integer repetitions) {
    this.repetitions = repetitions;
  }

  public Site maximumPortions(Integer maximumPortions) {
    this.maximumPortions = maximumPortions;
    return this;
  }

  /**
   * Get maximumPortions
   * @return maximumPortions
  **/
  @ApiModelProperty(example = "5", value = "")
  
    public Integer getMaximumPortions() {
    return maximumPortions;
  }

  public void setMaximumPortions(Integer maximumPortions) {
    this.maximumPortions = maximumPortions;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Site site = (Site) o;
      return Objects.equals(this.name, site.name) &&
              Objects.equals(this.address, site.address) &&
              Objects.equals(this.zipCode, site.zipCode) &&
              Objects.equals(this.homepage, site.homepage) &&
              Objects.equals(this.repetitions, site.repetitions) &&
              Objects.equals(this.maximumPortions, site.maximumPortions);
  }

  @Override
  public int hashCode() {
      return Objects.hash(name, address, zipCode, homepage, repetitions, maximumPortions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Site {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    address: ").append(toIndentedString(address)).append("\n");
      sb.append("    zipCode: ").append(toIndentedString(zipCode)).append("\n");
      sb.append("    homepage: ").append(toIndentedString(homepage)).append("\n");
    sb.append("    repetitions: ").append(toIndentedString(repetitions)).append("\n");
    sb.append("    maximumPortions: ").append(toIndentedString(maximumPortions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
