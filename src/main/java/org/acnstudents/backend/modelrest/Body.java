package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Objects;

/**
 * Body
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Body {
  @JsonProperty("site")
  private Site site = null;

  @JsonProperty("siteManager")
  private SiteManager siteManager = null;

  public Body site(Site site) {
    this.site = site;
    return this;
  }

  /**
   * Get site
   *
   * @return site
   **/
  @ApiModelProperty(value = "")

  @Valid
  public Site getSite() {
    return site;
  }

  public void setSite(Site site) {
    this.site = site;
  }

  public Body siteManager(SiteManager siteManager) {
    this.siteManager = siteManager;
    return this;
  }

  /**
   * Get siteManager
   *
   * @return siteManager
   **/
  @ApiModelProperty(value = "")

  @Valid
  public SiteManager getSiteManager() {
    return siteManager;
  }

  public void setSiteManager(SiteManager siteManager) {
    this.siteManager = siteManager;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Body body = (Body) o;
    return Objects.equals(this.site, body.site) &&
            Objects.equals(this.siteManager, body.siteManager);
  }

  @Override
  public int hashCode() {
    return Objects.hash(site, siteManager);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Body {\n");

    sb.append("    site: ").append(toIndentedString(site)).append("\n");
    sb.append("    siteManager: ").append(toIndentedString(siteManager)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
