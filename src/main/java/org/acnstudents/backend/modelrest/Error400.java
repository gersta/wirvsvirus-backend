package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import org.threeten.bp.OffsetDateTime;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Error400
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Error400   {
  @JsonProperty("statusCode")
  private Integer statusCode = null;

  @JsonProperty("requestUrl")
  private String requestUrl = null;

  @JsonProperty("requestTimestamp")
  private OffsetDateTime requestTimestamp = null;

  @JsonProperty("parameterErrors")
  @Valid
  private List<Error400ParameterErrors> parameterErrors = null;

  public Error400 statusCode(Integer statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  /**
   * Get statusCode
   * @return statusCode
  **/
  @ApiModelProperty(example = "400", value = "")
  
    public Integer getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(Integer statusCode) {
    this.statusCode = statusCode;
  }

  public Error400 requestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
    return this;
  }

  /**
   * Get requestUrl
   * @return requestUrl
  **/
  @ApiModelProperty(example = "https://...", value = "")
  
    public String getRequestUrl() {
    return requestUrl;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

  public Error400 requestTimestamp(OffsetDateTime requestTimestamp) {
    this.requestTimestamp = requestTimestamp;
    return this;
  }

  /**
   * Get requestTimestamp
   * @return requestTimestamp
  **/
  @ApiModelProperty(value = "")
  
    @Valid
    public OffsetDateTime getRequestTimestamp() {
    return requestTimestamp;
  }

  public void setRequestTimestamp(OffsetDateTime requestTimestamp) {
    this.requestTimestamp = requestTimestamp;
  }

  public Error400 parameterErrors(List<Error400ParameterErrors> parameterErrors) {
    this.parameterErrors = parameterErrors;
    return this;
  }

  public Error400 addParameterErrorsItem(Error400ParameterErrors parameterErrorsItem) {
    if (this.parameterErrors == null) {
      this.parameterErrors = new ArrayList<Error400ParameterErrors>();
    }
    this.parameterErrors.add(parameterErrorsItem);
    return this;
  }

  /**
   * Get parameterErrors
   * @return parameterErrors
  **/
  @ApiModelProperty(value = "")
      @Valid
    public List<Error400ParameterErrors> getParameterErrors() {
    return parameterErrors;
  }

  public void setParameterErrors(List<Error400ParameterErrors> parameterErrors) {
    this.parameterErrors = parameterErrors;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error400 error400 = (Error400) o;
    return Objects.equals(this.statusCode, error400.statusCode) &&
        Objects.equals(this.requestUrl, error400.requestUrl) &&
        Objects.equals(this.requestTimestamp, error400.requestTimestamp) &&
        Objects.equals(this.parameterErrors, error400.parameterErrors);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusCode, requestUrl, requestTimestamp, parameterErrors);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error400 {\n");
    
    sb.append("    statusCode: ").append(toIndentedString(statusCode)).append("\n");
    sb.append("    requestUrl: ").append(toIndentedString(requestUrl)).append("\n");
    sb.append("    requestTimestamp: ").append(toIndentedString(requestTimestamp)).append("\n");
    sb.append("    parameterErrors: ").append(toIndentedString(parameterErrors)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
