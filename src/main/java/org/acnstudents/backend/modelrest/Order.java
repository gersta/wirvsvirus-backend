package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * Order
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Order {
    @JsonProperty("slotID")
    private Integer slotID = null;

    @JsonProperty("comment")
    private String comment = null;

    @JsonProperty("customerId")
    private Integer customerid = null;

//  @JsonProperty("confirmed")
//  private Boolean confirmed = null;

    @JsonProperty("collector")
    private String collector = null;

    @JsonProperty("customer")
    private Customer customer = null;

    @JsonProperty("timeslot")
    private Timeslot timeslot = null;

    public Order slotID(Integer slotID) {
        this.slotID = slotID;
        return this;
    }


    /**
     * Get slotID
     *
     * @return slotID
     **/
    @ApiModelProperty(example = "28", value = "")

    public Integer getSlotID() {
        return slotID;
    }

    public void setSlotID(Integer slotID) {
        this.slotID = slotID;
    }

    public Order comment(String comment) {
        this.comment = comment;
        return this;
    }

    /**
     * Get comment
     *
     * @return comment
     **/
    @ApiModelProperty(example = "Gemüse", value = "")

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


    public Order collector(String collector) {
        this.collector = collector;
        return this;
    }

    /**
     * Get collector
     *
     * @return collector
     **/
    @ApiModelProperty(example = "TBD", value = "")

    public String getCollector() {
        return collector;
    }

    public void setCollector(String collector) {
        this.collector = collector;
    }

    public Integer getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Integer customerid) {
        this.customerid = customerid;
    }

    public Order getCustomerId(Integer customerid) {
        this.customerid = customerid;
        return this;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Timeslot getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(Timeslot timeslot) {
        this.timeslot = timeslot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(slotID, order.slotID) &&
                Objects.equals(comment, order.comment) &&
                Objects.equals(customerid, order.customerid) &&
                Objects.equals(collector, order.collector) &&
                Objects.equals(customer, order.customer) &&
                Objects.equals(timeslot, order.timeslot);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slotID, comment, customerid, collector, customer, timeslot);
    }

    @Override
    public String toString() {
        return "Order{" +
                "slotID=" + slotID +
                ", comment='" + comment + '\'' +
                ", customerid=" + customerid +
                ", collector='" + collector + '\'' +
                ", customer=" + customer +
                ", timeslot=" + timeslot +
                '}';
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
