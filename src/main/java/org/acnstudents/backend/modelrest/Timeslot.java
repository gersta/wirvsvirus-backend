package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import org.threeten.bp.OffsetDateTime;

import javax.validation.Valid;
import java.util.Objects;

/**
 * Slot
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Timeslot {
    @JsonProperty("siteId")
    private Integer siteId = null;

    @JsonProperty("maximumPersons")
    private Integer maximumPersons = null;

    @JsonProperty("currentPersons")
    private Integer currentPersons = null;

    @JsonProperty("startTime")
    private OffsetDateTime startTime = null;

    @JsonProperty("endTime")
    private OffsetDateTime endTime = null;

    public Timeslot(Integer siteId, Integer maximumPersons, Integer currentpersons, OffsetDateTime startTime, OffsetDateTime endTime) {
        this.siteId = siteId;
        this.maximumPersons = maximumPersons;
        this.currentPersons = currentpersons;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Timeslot() {
    }

    public Timeslot siteId(Integer siteId) {
        this.siteId = siteId;
        return this;
    }

    public Timeslot(Integer maximumPersons, OffsetDateTime startTime, OffsetDateTime endTime) {
        this.maximumPersons = maximumPersons;
        this.maximumPersons = maximumPersons;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    /**
     * Get siteId
     *
     * @return siteId
     **/
    @ApiModelProperty(example = "13", value = "")

    public Integer getSiteID() {
        return siteId;
    }

    public void setSiteID(Integer siteId) {
        this.siteId = siteId;
    }

    public Timeslot maximumPersons(Integer maximumPersons) {
        this.maximumPersons = maximumPersons;
        return this;
    }

    /**
     * Get maximumPersons
     *
     * @return maximumPersons
     **/
    @ApiModelProperty(example = "5", value = "")

    public Integer getMaximumPersons() {
        return maximumPersons;
    }

    public void setMaximumPersons(Integer maximumPersons) {
        this.maximumPersons = maximumPersons;
    }

    public Timeslot startTime(OffsetDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public Integer getCurrentPersons() {
        return currentPersons;
    }

    public void setCurrentPersons(Integer currentPersons) {
        this.currentPersons = currentPersons;
    }

    /**
     * Get startTime
     *
     * @return startTime
     **/
    @ApiModelProperty(example = "2020-03-21T13:08:28Z", value = "")

    @Valid
    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(OffsetDateTime startTime) {
        this.startTime = startTime;
    }

    public Timeslot endTime(OffsetDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * Get endTime
     *
     * @return endTime
     **/
    @ApiModelProperty(example = "2020-03-21T23:36:58Z", value = "")

    @Valid
    public OffsetDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(OffsetDateTime endTime) {
        this.endTime = endTime;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Timeslot timeslot = (Timeslot) o;
        return Objects.equals(this.siteId, timeslot.siteId) &&
                Objects.equals(this.maximumPersons, timeslot.maximumPersons) &&
                Objects.equals(this.currentPersons, timeslot.currentPersons) &&
                Objects.equals(this.startTime, timeslot.startTime) &&
                Objects.equals(this.endTime, timeslot.endTime);
    }

  @Override
  public String toString() {
    return "Timeslot{" +
            "siteId=" + siteId +
            ", maximumPersons=" + maximumPersons +
            ", currentPersons=" + currentPersons +
            ", startTime=" + startTime +
            ", endTime=" + endTime +
            '}';
  }

  @Override
  public int hashCode() {
      return Objects.hash(siteId, maximumPersons, currentPersons, startTime, endTime);
  }

  /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
