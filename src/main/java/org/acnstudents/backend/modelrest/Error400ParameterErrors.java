package org.acnstudents.backend.modelrest;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * Error400ParameterErrors
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-03-21T23:31:46.144Z[GMT]")
public class Error400ParameterErrors {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("messsage")
  private String messsage = null;

  public Error400ParameterErrors name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "user.name", value = "")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Error400ParameterErrors messsage(String messsage) {
    this.messsage = messsage;
    return this;
  }

  /**
   * Get messsage
   * @return messsage
  **/
  @ApiModelProperty(example = "Must not contain null value", value = "")
  
    public String getMesssage() {
    return messsage;
  }

  public void setMesssage(String messsage) {
    this.messsage = messsage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error400ParameterErrors error400ParameterErrors = (Error400ParameterErrors) o;
    return Objects.equals(this.name, error400ParameterErrors.name) &&
        Objects.equals(this.messsage, error400ParameterErrors.messsage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, messsage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error400ParameterErrors {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    messsage: ").append(toIndentedString(messsage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
