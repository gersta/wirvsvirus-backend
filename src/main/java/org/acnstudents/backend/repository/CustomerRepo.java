package org.acnstudents.backend.repository;

import org.acnstudents.backend.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<Customer, Integer> {
    Customer findOneByEmail(String email);
}
