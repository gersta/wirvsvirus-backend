package org.acnstudents.backend.repository;
import org.acnstudents.backend.model.User;
import org.springframework.data.repository.CrudRepository;


public interface UserRepo extends CrudRepository<User, Integer> {
    User findOneByEmail(String email);
}