package org.acnstudents.backend.repository;

import org.acnstudents.backend.model.Site;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SiteRepo extends CrudRepository<Site, Integer> {


    List<Site> findAll();

    List<Site> findAllByZipCode(String zipCode);

    List<Site> findAllByZipCodeStartsWith(String zipCode);
}
