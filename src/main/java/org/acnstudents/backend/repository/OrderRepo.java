package org.acnstudents.backend.repository;

import org.acnstudents.backend.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Order, Integer> {
    Integer countBySlotId(Integer id);
}
