package org.acnstudents.backend.repository;

import org.acnstudents.backend.model.Site;
import org.acnstudents.backend.model.Slot;
import org.springframework.data.repository.CrudRepository;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

public interface SlotRepo extends CrudRepository<Slot, Integer>{

    List<Slot> findAllByStartAfterAndSiteOrderByStartAsc(Timestamp timestamp, Site site);

}
