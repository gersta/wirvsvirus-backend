package org.acnstudents.backend.repository;

import org.acnstudents.backend.model.SiteManager;
import org.springframework.data.repository.CrudRepository;

public interface SiteManagerRepo extends CrudRepository<SiteManager, Integer> {

    Integer countAllByEmail(String email);
    SiteManager findOneByEmail(String email);
}
