package org.acnstudents.backend;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class TimezoneTest {

    @Test
    public void tzTest() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        OffsetDateTime o = OffsetDateTime.ofInstant(now.toInstant(), ZoneId.systemDefault());

        System.out.println(ZoneId.systemDefault());
    }

}
