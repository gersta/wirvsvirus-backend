package org.acnstudents.backend;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import static org.junit.Assert.*;

public class PasswordEncoderTest {

    @Test
    public void thisIsWrong(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();

        String hash1 = encoder.encode("password12345");
        String hash2 = encoder.encode("password12345");
        assertNotEquals(hash1, hash2);

    }

    @Test
    public void thisIsRight(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String hash1 = encoder.encode("password12345");
        encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.matches("password12345", hash1));
    }
}
